# DevOps Engineer Technical Test

Your task is to dockerize the `TF.DevOpsTest` project and build a Gitlab CI pipeline that deploys the project to your local machine. More specifically:

1. Fork this project. Ensure the forked project has a public access.
2. Dockerize the `TF.DevOpsTest` project.
3. Create a Gitlab CI YAML file.
4. Configure the CI to run `build`, `test`, and `release` jobs on all branches.
5. Configure the CI to dynamically set the `"AppConfig"."Environment"` values on the `appsettings.json` file for specific git branches.

   | Branch Name   | Environment Value |
   | :-------------|:------------------|
   | feature/*     | dev               |
   | bugfix/*      | dev               |
   | improvement/* | dev               |
   | qa/*          | uat               |
   | master/*      | prod              |
   
6. Validate by running the build and navigating to the `/color` endpoint. Ensure that the correct color values return for specific environment values.

   | Environment Value   | Color             |
   | :-------------------|:------------------|
   | dev                 | red               |
   | uat                 | blue              |
   | prod                | green             |
   | others              | white             |

# Submission
1. Create a public Gitlab repository and push your solution in it.
2. Send a short demo video of your solution and a link to your solution repository to `recruitment@touch-fire.com`.