﻿using System;

namespace TF.DevOpsTest
{
    public class ColorModel
    {
        public string Color { get; set; }
        public DateTime Date { get; set; }

        public ColorModel(string color)
        {
            Color = color;
            Date = DateTime.Now;
        }
    }
}
