﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using TF.DevOpsTest.Models;

namespace TF.DevOpsTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ColorController : Controller
    {
        private readonly IOptions<AppConfig> _config;

        public ColorController(IOptions<AppConfig> config)
        {
            _config = config;
        }

        [HttpGet]
        public IEnumerable<ColorModel> Get()
        {
            string color = "white";

            switch (_config.Value.Environment)
            {
                case "dev":
                    color = "red";
                    break;
                case "uat":
                    color = "blue";
                    break;
                case "prod":
                    color = "green";
                    break;
            }

            return new List<ColorModel>() { new ColorModel(color) };
        }
    }
}
