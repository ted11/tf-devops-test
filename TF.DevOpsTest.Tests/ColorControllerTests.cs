using Microsoft.Extensions.Options;
using NUnit.Framework;
using System.Collections.Generic;
using TF.DevOpsTest.Controllers;
using TF.DevOpsTest.Models;

namespace TF.DevOpsTest.Tests
{
    [TestFixture]
    public class ColorControllerTests
    {
        private ColorController _controller;
        private AppConfig _config;

        [SetUp]
        public void Setup()
        {
            _config = new AppConfig();
        }

        [Test]
        public void ItReturnsWhiteForLocalEnvironment()
        {
            _config.Environment = "local";
            IOptions<AppConfig> options = Options.Create(_config);
            _controller = new ColorController(options);

            List<ColorModel> colors = _controller.Get() as List<ColorModel>;

            Assert.AreEqual("white", colors[0].Color);
        }

        [Test]
        public void ItReturnsRedForDevEnvironment()
        {
            _config.Environment = "dev";
            IOptions<AppConfig> options = Options.Create(_config);
            _controller = new ColorController(options);

            List<ColorModel> colors = _controller.Get() as List<ColorModel>;

            Assert.AreEqual("red", colors[0].Color);
        }

        [Test]
        public void ItReturnsBlueForUatEnvironment()
        {
            _config.Environment = "uat";
            IOptions<AppConfig> options = Options.Create(_config);
            _controller = new ColorController(options);

            List<ColorModel> colors = _controller.Get() as List<ColorModel>;

            Assert.AreEqual("blue", colors[0].Color);
        }

        [Test]
        public void ItReturnsGreenForProdEnvironment()
        {
            _config.Environment = "prod";
            IOptions<AppConfig> options = Options.Create(_config);
            _controller = new ColorController(options);

            List<ColorModel> colors = _controller.Get() as List<ColorModel>;

            Assert.AreEqual("green", colors[0].Color);
        }
    }
}